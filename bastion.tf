resource "aws_security_group" "sshbastion" {
  vpc_id = "${aws_vpc.defaultVpc.id}"
  name = "sshbastion"
  ingress{
        cidr_blocks = ["0.0.0.0/0"]
        to_port = "22"
        from_port = "22"
        protocol = "tcp"
  }

  egress{
        cidr_blocks = ["0.0.0.0/0"]
        to_port = "0"
        from_port = "0"
        protocol = "-1"
  }
}

resource "tls_private_key" "keyssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "keybastionssh" {
  key_name = "keybastionssh"
  public_key = "${tls_private_key.keyssh.public_key_openssh}"
}

resource "aws_instance" "intancebastion" {
  ami= "${lookup(var.amis, lookup(var.awsconfig, "region"))}"
  subnet_id = "${aws_subnet.publica.id}"
  instance_type = "t2.micro"
  key_name      = "${aws_key_pair.keybastionssh.key_name}"
  vpc_security_group_ids = ["${aws_security_group.sshbastion.id}"]
  depends_on = ["aws_db_instance.mysqlwordpress"]
  user_data = "${file("installbd.sh")}"
 
}


resource "aws_eip" "eippublicssh" {
  instance = "${aws_instance.intancebastion.id}"
}
