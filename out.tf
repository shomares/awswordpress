output "eipsshpublic" {
  value = "${aws_eip.eippublicssh.public_ip}"
}

output "sshkey" {
  value = "${tls_private_key.keyssh.private_key_pem}"
}

output "dnselb" {
  value = "${aws_elb.elasticloadbalancer.dns_name}"
}