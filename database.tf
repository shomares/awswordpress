resource "aws_security_group" "mysqlgroupnames" {
    vpc_id = "${aws_vpc.defaultVpc.id}"
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        to_port = "3306"
        from_port = "3306"
        protocol = "tcp"
    }
    egress{
        cidr_blocks = ["0.0.0.0/0"]
        to_port = "0"
        from_port = "0"
        protocol = "-1"
    }
}


resource "aws_db_subnet_group" "rdssubnetgp" {
    name = "rdssubnetgp"
    description = "Our main group of subnets"
    subnet_ids = ["${aws_subnet.privateb.id}", "${aws_subnet.privatea.id}"]
}

resource "aws_db_instance" "mysqlwordpress" {
    engine = "mysql"
    db_subnet_group_name = "${aws_db_subnet_group.rdssubnetgp.id}"
    password = "ArdillaManUnt11"
    username = "admin"
    name = "awsmysqlwordpressdb"
    vpc_security_group_ids = ["${aws_security_group.mysqlgroupnames.id}"]
    instance_class= "db.t2.micro"
    allocated_storage    = 20
    storage_type         = "gp2"
    identifier = "awsmysqlwordpressdb"
    skip_final_snapshot = true
 
}

