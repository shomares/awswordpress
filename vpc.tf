resource "aws_vpc" "defaultVpc" {
  cidr_block= "${var.vpccidr}"
}

resource "aws_internet_gateway" "internetGateway" {
  vpc_id= "${aws_vpc.defaultVpc.id}"
  
}

//Subnets table--------------------------------------------
resource "aws_subnet" "publica" {
    vpc_id = "${aws_vpc.defaultVpc.id}"
    cidr_block = "${element(lookup(var.subnetscidr, "public"),0)}"
    availability_zone = "${element(lookup(var.avialibityzones, "public"),0)}"
}

resource "aws_subnet" "publicb" {
    vpc_id = "${aws_vpc.defaultVpc.id}"
    cidr_block = "${element(lookup(var.subnetscidr, "public"),1)}"
    availability_zone = "${element(lookup(var.avialibityzones, "public"),1)}"

}
resource "aws_subnet" "privatea" {
    vpc_id = "${aws_vpc.defaultVpc.id}"
    cidr_block = "${element(lookup(var.subnetscidr, "private"),0)}"
    availability_zone = "${element(lookup(var.avialibityzones, "private"), 0)}"
}

resource "aws_subnet" "privateb" {
    vpc_id = "${aws_vpc.defaultVpc.id}"
    cidr_block = "${element(lookup(var.subnetscidr, "private"),1)}"
    availability_zone = "${element(lookup(var.avialibityzones, "private"), 1)}"

}

resource "aws_eip" "eipgateway" {
}


//RouteTables------------------------------------------------------
resource "aws_nat_gateway" "natgateway" {
    subnet_id= "${aws_subnet.publica.id}"
    allocation_id = "${aws_eip.eipgateway.id}"
}

resource "aws_route_table" "routepublic" {
    vpc_id = "${aws_vpc.defaultVpc.id}"
    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.internetGateway.id}"
    }
}

resource "aws_route_table" "routeprivate" {
  vpc_id = "${aws_vpc.defaultVpc.id}"
    route {
        cidr_block= "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.natgateway.id}" 
    }
}

resource "aws_route_table_association" "routeassocationpublic1" {
  route_table_id= "${aws_route_table.routepublic.id}"
  subnet_id = "${aws_subnet.publica.id}"
}

resource "aws_route_table_association" "routeassocationpublic2" {
    route_table_id = "${aws_route_table.routepublic.id}"
    subnet_id = "${aws_subnet.publicb.id}"
  
}

resource "aws_route_table_association" "routeassocationprivate1" {
  route_table_id= "${aws_route_table.routeprivate.id}"
  subnet_id = "${aws_subnet.privatea.id}"
}

resource "aws_route_table_association" "routeassocationprivate2" {
  route_table_id= "${aws_route_table.routeprivate.id}"
  subnet_id = "${aws_subnet.privateb.id}"
}

