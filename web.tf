resource "aws_security_group" "httpsecurityinternal" {
    vpc_id = "${aws_vpc.defaultVpc.id}"
    ingress {
        cidr_blocks = ["0.0.0.0/0"]
        to_port = "80"
        from_port = "80"
        protocol = "tcp"
    }
    egress{
        cidr_blocks = ["0.0.0.0/0"]
        to_port = "0"
        from_port = "0"
        protocol = "-1"
    }
}

resource "aws_security_group" "httpsecurity" {
    vpc_id = "${aws_vpc.defaultVpc.id}"
    ingress {
        cidr_blocks = ["189.216.67.61/32"]
        to_port = "80"
        from_port = "80"
        protocol = "tcp"
    }
    egress{
        cidr_blocks = ["0.0.0.0/0"]
        to_port = "0"
        from_port = "0"
        protocol = "-1"
    }
 
}

resource "aws_elb" "elasticloadbalancer" {
    name = "wordpressloadbalancer"
    subnets = ["${aws_subnet.publica.id}","${aws_subnet.publicb.id}"]
    security_groups = ["${aws_security_group.httpsecurity.id}"]
    health_check {
            healthy_threshold = 2
            unhealthy_threshold = 10
            timeout = 40
            interval = 60
            target = "HTTP:80/wp-admin/css/forms.min.css?ver=5.2.4"

    }
    listener {
        lb_port = 80
        lb_protocol = "http"
        instance_port = "80"
        instance_protocol = "http"
    }
}

resource "aws_launch_configuration" "lauchconfiguration1" {
  instance_type = "t2.small"
  user_data = "${file("install.sh")}"
  security_groups = ["${aws_security_group.httpsecurityinternal.id}"]
  image_id = "${lookup(var.amis, lookup(var.awsconfig, "region"))}"   
  depends_on= ["aws_db_instance.mysqlwordpress"]
}

resource "aws_autoscaling_group" "autoscaling" {
    launch_configuration = "${aws_launch_configuration.lauchconfiguration1.id}"
    load_balancers = ["${aws_elb.elasticloadbalancer.id}"]
    vpc_zone_identifier = ["${aws_subnet.privatea.id}", "${aws_subnet.privateb.id}"]
    desired_capacity = 1
    min_size = 1
    max_size = 3
    health_check_type = "ELB"
    tag {
        key = "Name"
        value = "wordpress-instances"
        propagate_at_launch = true
  }
}


