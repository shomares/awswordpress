provider "aws" {
  region = "${lookup(var.awsconfig, "region")}"
  access_key = "${lookup(var.awsconfig, "access_key")}"
  secret_key = "${lookup(var.awsconfig, "secret_key")}"
}
